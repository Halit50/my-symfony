<?php 

use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

//ce code remplace le $map d'en dessous gràace au bundle de routing
//composer require symfony/routing
$routes = new RouteCollection();
$routes->add('hello', new Route('/hello/{name}', ['name' => 'World']));
$routes->add('bye', new Route('/bye'));
$routes->add('cms/about', new Route('/a-propos'));

return $routes;