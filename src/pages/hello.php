<?php
// Au lieu d'écrire la ligne en dessous
// $name = isset($_GET['name']) ? $_GET['name'] : 'World';

// on refactorise notre code en écrivant:
// ce bout de code ne sert également plus car dans le index.php, nous avons fait un extract de $request->query->all()
//$name = $request->query->get('name', 'World');
?>

Hello <?= htmlspecialchars($name, ENT_QUOTES)?>