<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;

require __DIR__.'/../vendor/autoload.php';

//Comment fait on la diff entre une personne qui veut la page hello
// et celle qui veut la page bye

$request = Request::createFromGlobals();

$response = new Response();

$routes = require __DIR__ . '/../src/routes.php';

$context = new RequestContext();
$context->fromRequest($request);

$urlMatcher = new UrlMatcher($routes, $context);

// $map = [
//     '/hello' => 'hello.php',
//     '/bye' => 'bye.php',
//     '/a-propos' => '/cms/about.php'
// ];

// récupérer l'url après l'index.php
$pathInfo = $request->getPathInfo();



//var_dump($resultat); die();

try {
    //$resultat = $urlMatcher->match($pathInfo);
    extract($urlMatcher->match($pathInfo));
    //extract($request->query->all());
    ob_start();
    include __DIR__.'/../src/pages/'.$resultat['_route'].'.php';
    $response->setContent(ob_get_clean());
} catch(ResourceNotFoundException $e) {
    $response->setContent("La page demandée n'existe pas");
    $response->setStatusCode(404);
} catch(Exception $e) {
    $response->setContent("Une erreur est survenue");
    $response->setStatusCode(500);
}

// if (isset($map[$pathInfo])){
//     extract($request->query->all());
//     ob_start();
//     include __DIR__.'/../src/pages/'.$map[$pathInfo];
//     $response->setContent(ob_get_clean());
// } else {
//     $response->setContent("La page demandée n'existe pas");
//     $response->setStatusCode(404);
// }

$response->send();